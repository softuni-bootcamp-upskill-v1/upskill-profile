package com.example.demo.service.impl;

import com.example.demo.model.entity.CompanyEntity;
import com.example.demo.model.view.ClientsViewModel;
import com.example.demo.model.view.CompanyView;
import com.example.demo.repository.CompanyRepository;
import com.example.demo.service.CompanyService;
import com.example.demo.stream.ProfileStream;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;
    private final ModelMapper modelMapper;
    private final ProfileStream profileStream;

    public CompanyServiceImpl(CompanyRepository companyRepository, ModelMapper modelMapper, ProfileStream profileStream) {
        this.companyRepository = companyRepository;
        this.modelMapper = modelMapper;
        this.profileStream = profileStream;
    }


    @Override
    public List<CompanyView> getAllCompanies(int page, int limit) {

        if (page > 0) {
            page = page - 1;
        }

        Pageable pageableRequest = PageRequest.of(page, limit);

        return this.companyRepository
                .findAll(pageableRequest)
                .map(this::mapToCompanyView)
                .stream()
                .sorted(Comparator.comparing(CompanyView::getName)
                        .reversed()
                        .thenComparing(CompanyView::getEmail)
                        .reversed())
                .collect(Collectors.toList());

    }

    @Override
    public boolean checkCompanyName(String companyName) {
        return this.companyRepository.findByName(companyName).isPresent();
    }

    @Override
    public <Т> void sendMessageOutBoundCompanyNameCheck(Т kafkaModel) {
        MessageChannel messageChannel = this.profileStream.outboundCompanyNameCheck();
        messageChannel.send(MessageBuilder
                .withPayload(kafkaModel)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }

    @Override
    public ClientsViewModel findClientsForAdminDashboard() {
        LocalDate dateNow = LocalDate.now();
        int year = dateNow.getYear();
        int month = dateNow.getMonth().getValue();

        int count = this.companyRepository.countByCreated_MonthValueAndCreated_Year(month, year);
        ClientsViewModel clients = new ClientsViewModel();

        List<Integer> counts = new ArrayList<>();
        List<String> months = new ArrayList<>();
        List<Integer> monthsByNumber = new ArrayList<>();

        counts.add(count);
        monthsByNumber.add(month);
        months.add(getMonth(month));

        month--;
        for (int i = 0; i < 6; i++) {

            if (month == 0) {
                month = 12;
                year = year - 1;
                pureClients(year, month, counts, months, monthsByNumber);
            } else {
                pureClients(year, month, counts, months, monthsByNumber);
            }
            month--;
        }

        Collections.reverse(counts);
        Collections.reverse(months);
        Collections.reverse(monthsByNumber);

        clients.setCounts(counts);
        clients.setMonthByNumber(monthsByNumber);
        clients.setMonths(months);
        return clients;
    }

    @Override
    public List<CompanyView> findClients(int page, int size) {
        page--;
        Pageable pages = PageRequest.of(page, size);

        return this.companyRepository.findAll(pages)
                .getContent()
                .stream().map(s -> this.modelMapper.map(s, CompanyView.class)).toList();
    }

    private void pureClients(int year, int month, List<Integer> counts, List<String> months, List<Integer> monthsByNumber) {
        int countPure = this.companyRepository.countByCreated_MonthValueAndCreated_Year(month, year);
        if (countPure > 0) {
            months.add(getMonth(month));
            counts.add(countPure);
            monthsByNumber.add(month);
        }
    }

    private String getMonth(int month) {
        return switch (month) {
            case 1 -> "January";
            case 2 -> "February";
            case 3 -> "Mart";
            case 4 -> "April";
            case 5 -> "May";
            case 6 -> "June";
            case 7 -> "July";
            case 8 -> "August";
            case 9 -> "September";
            case 10 -> "October";
            case 11 -> "November";
            default -> "December";
        };
    }


    private CompanyView mapToCompanyView(CompanyEntity company) {
        CompanyView mapped = modelMapper.map(company, CompanyView.class);

        return mapped;
    }
}
