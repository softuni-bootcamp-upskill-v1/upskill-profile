package com.example.demo.model.binding;

public class DemoRequestBindingModel {

    private String email;
    private String videoUrl;

    public DemoRequestBindingModel() {
    }

    public String getEmail() {
        return email;
    }

    public DemoRequestBindingModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public DemoRequestBindingModel setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
        return this;
    }
}
