package com.example.demo.model.view;

import java.util.List;

public class ClientsViewModel {

    private List<Integer> counts;
    private List<String> months;
    private List<Integer> monthByNumber;

    public ClientsViewModel() {
    }

    public List<Integer> getCounts() {
        return counts;
    }

    public ClientsViewModel setCounts(List<Integer> counts) {
        this.counts = counts;
        return this;
    }

    public List<String> getMonths() {
        return months;
    }

    public ClientsViewModel setMonths(List<String> months) {
        this.months = months;
        return this;
    }

    public List<Integer> getMonthByNumber() {
        return monthByNumber;
    }

    public ClientsViewModel setMonthByNumber(List<Integer> monthByNumber) {
        this.monthByNumber = monthByNumber;
        return this;
    }
}
