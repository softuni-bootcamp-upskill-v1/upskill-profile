package com.example.demo.repository;

import com.example.demo.model.entity.CompanyEntity;
import com.example.demo.model.entity.UserProfileEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.Month;
import java.util.List;
import java.util.Optional;


@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, String> {

    @Transactional(readOnly = true)
    Page<CompanyEntity> findAll(Pageable page);

    Optional<CompanyEntity> findByName(String name);

    Optional<CompanyEntity> findCompanyEntityByCompanyOwner(String email);

    Optional<CompanyEntity> findCompanyEntityByEmail(String email);


    @Query("select c.id from CompanyEntity as c where c.email=?1")
    String getCompanyId(String companyOwnerEmail);




    // findByCreated_MonthValueAndCreated_Year
    @Query("select count (c) from CompanyEntity c where month(c.created) = ?1 and year(c.created) = ?2 ")
   int countByCreated_MonthValueAndCreated_Year(int month, int year);
}
