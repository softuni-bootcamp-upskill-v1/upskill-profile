package com.example.demo.error;

public abstract class CustomEx extends RuntimeException {
    public CustomEx(String message) {
        super(message);
    }
}
